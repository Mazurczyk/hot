import java.io.*;
import java.util.*;

public class Krolik {

    private static final String COMMA_DELIMITER = ",";

    public static void main(String[] args) {
        List<String[]> records = getCSV();
        String columnName;
        if (isValidData(args, records)) {
            columnName = "{" + args[0] + "}";
            System.out.println(getColumnSum(records, columnName));
        } else {
            printKlops();
            return;
        }
    }

    private static List<String[]> getCSV() {
        List<String[]> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(values);
            }
        } catch (IOException e) {
            printKlops();
        }
        return records;
    }

    private static boolean isValidColumn(List<String[]> records, String columnName) {
        Iterator<String[]> rowIterator = records.iterator();
        String[] row;
        int occurrence = 0;
        while (rowIterator.hasNext()) {
            row = rowIterator.next();
            for (String data : row) {
                if (data.equals(columnName)) {
                    occurrence++;
                }
            }
            if (occurrence > 1) return false;
        }
        if (occurrence == 1) return true;
        else return false;
    }

    private static boolean isValidNumber(String data) {
        try {
            int numberData = Integer.parseInt(data);
            if (numberData > -10000 && numberData < 10000) return true;
            else return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static long getColumnSum(List<String[]> records, String columnName) {
        long columnSum = 0;
        String[] row;
        Iterator<String[]> rowIterator = records.iterator();
        int column = getColumn(records, columnName);
        while (rowIterator.hasNext()) {
            row = rowIterator.next();
            if (isValidNumber(row[column])) {
                columnSum += Integer.parseInt(row[column]);
            }
        }
        return columnSum;
    }

    private static int getColumn(List<String[]> records, String columnName) {
        Iterator<String[]> rowIterator = records.iterator();
        String[] row;
        int column = 0;
        while (rowIterator.hasNext()) {
            row = rowIterator.next();
            for (String data : row) {
                if (data.equals(columnName)) {
                    return column;
                }
                column++;
            }
            column = 0;
        }
        return column;
    }

    private static void printKlops() {
        System.out.println("klops");
    }

    private static boolean isValidData(String[] args, List<String[]> records) {
        if (isValidSize(args) && isValidColumn(records, args[0])) return true;
        else return false;
    }

    private static boolean isValidSize(String[] args) {
        if (args.length == 1) return true;
        else return false;
    }
}
