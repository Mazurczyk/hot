import java.util.Arrays;

public class Galaktyka {

    public static void main(String[] args) {
        if (isValidData(args)) {
            int telescopeSize = Integer.parseInt(args[0]);
            char orientation = args[1].toCharArray()[0];
            printGalaxy(telescopeSize, orientation);
            System.out.println(getSpiralLength(telescopeSize));
        } else {
            printKlops();
            return;
        }
    }

    private static int getSpiralLength(int telescopeSize) {
        int spiralLength = 3;
        for (int i = 2; i <= telescopeSize; i++) {
            spiralLength += i + 1;
        }
        return spiralLength;
    }

    private static void printGalaxy(int telescopeSize, char orientation) {
        Character[][] galaxy = getNewGalaxy(telescopeSize, orientation);
        MovingData movingData = new MovingData(orientation, galaxy);
        for (int i = 0; i < (galaxy.length * galaxy[0].length) - getSpiralLength(telescopeSize); i++) {
            galaxy[movingData.getRow()][movingData.getCol()] = '*';
            movingData.movePosition(galaxy);
        }
        printGalaxy(galaxy);
    }

    private static Character[][] getNewGalaxy(int telescopeSize, char orientation) {
        if (orientation == 'W' || orientation == 'E')
            return new Character[telescopeSize + 3][telescopeSize + 2];
        else return new Character[telescopeSize + 2][telescopeSize + 3];
    }

    private static void printGalaxy(Character[][] galaxy) {
        for (Character[] ch : galaxy) {
            String row = Arrays.toString(ch);
            System.out.println(row.substring(1, row.length() - 1).replaceAll("null", " ").replaceAll(", ", ""));
        }
    }

    private static boolean isValidData(String[] args) {
        if (isValidSize(args) && isValidNumber(args[0]) && isValidChar(args[1].toCharArray()[0])) return true;
        else return false;
    }

    private static boolean isValidChar(char arg) {
        if ((int) arg == 78 || (int) arg == 69 || (int) arg == 83 || (int) arg == 87) return true;
        else return false;
    }

    private static boolean isValidSize(String[] args) {
        if (args.length != 2 || args[1].length() != 1) return false;
        else return true;
    }

    private static boolean isValidInteger(String arg) {
        try {
            Integer.parseInt(arg);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isValidNumber(String arg) {
        if (!isValidInteger(arg) || Integer.parseInt(arg) > 100000 || Integer.parseInt(arg) <= 0) return false;
        return true;
    }

    private static void printKlops() {
        System.out.println("klops");
    }

    private static class MovingData {
        private char move;
        private int row;
        private int col;
        private int rowBoundary;
        private int colBoundary;

        public MovingData(char orientation, Character[][] galaxy) {
            rowBoundary = galaxy.length - 1;
            colBoundary = galaxy[0].length - 1;
            switch (orientation) {
                case 'N':
                    move = 'd';
                    row = 0;
                    col = colBoundary;
                    break;
                case 'E':
                    move = 'l';
                    row = rowBoundary;
                    col = colBoundary;
                    break;
                case 'S':
                    move = 'u';
                    row = rowBoundary;
                    col = 0;
                    break;
                case 'W':
                    move = 'r';
                    row = 0;
                    col = 0;
                    break;
            }
        }

        private void movePosition(Character[][] galaxy) {
            switch (move) {
                case 'd':
                    if (!isTurn(move, galaxy)) {
                        row++;
                    } else {
                        move = 'l';
                        col--;
                    }
                    break;
                case 'l':
                    if (!isTurn(move, galaxy)) {
                        col--;
                    } else {
                        move = 'u';
                        row--;
                    }
                    break;
                case 'u':
                    if (!isTurn(move, galaxy)) {
                        row--;
                    } else {
                        move = 'r';
                        col++;
                    }
                    break;
                case 'r':
                    if (!isTurn(move, galaxy)) {
                        col++;
                    } else {
                        move = 'd';
                        row++;
                    }
                    break;
            }
        }

        private boolean isTurn(char move, Character[][] galaxy) {
            switch (move) {
                case 'd':
                    if (row <= rowBoundary - 2 && galaxy[row + 2][col] != null) return true;
                    else if (row == rowBoundary) return true;
                    else return false;
                case 'l':
                    if (col >= 2 && galaxy[row][col - 2] != null) return true;
                    else if (col == 0) return true;
                    else return false;
                case 'u':
                    if (row >= 2 && galaxy[row - 2][col] != null) return true;
                    else if (row == 0) return true;
                    else return false;
                case 'r':
                    if (col <= colBoundary - 2 && galaxy[row][col + 2] != null) return true;
                    else if (col == colBoundary) return true;
                    else return false;
                default:
                    return false;
            }
        }

        public char getMove() {
            return move;
        }

        public void setMove(char move) {
            this.move = move;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getCol() {
            return col;
        }

        public void setCol(int col) {
            this.col = col;
        }
    }
}
