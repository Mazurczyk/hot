import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Wilk {

    private static final String SPACE_DELIMITER = " ";

    public static void main(String[] args) {
        List<String[]> records = getCSV();
        if (isValidData(records)) {
            double fuel = getFuel(records);
            List<Double> tapYield = getTapYield(records);
            List<Double> temperature = getTemperature(records);
            System.out.println(getLoadingTime(fuel, tapYield));
            System.out.println(getAverageTemperature(temperature));
        } else {
            printKlops();
            return;
        }
    }

    private static List<String[]> getCSV() {
        List<String[]> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(SPACE_DELIMITER);
                records.add(values);
            }
        } catch (IOException e) {
            System.out.println("klops");
        }
        return records;
    }

    private static double getFuel(List<String[]> records) {
        return roundData(Double.parseDouble(records.get(0)[0]));
    }

    private static double roundData(double data) {
        return new BigDecimal(Double.toString(data)).setScale(5, RoundingMode.HALF_UP).doubleValue();
    }

    private static boolean isValidDouble(String data) {
        try {
            Double.parseDouble(data);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isValidFuel(List<String[]> records) {
        String data = records.get(0)[0];
        double fuel;
        if (!isValidDouble(data)) return false;
        else {
            fuel = Double.parseDouble(data);
            if (fuel <= 0 && fuel >= 100000) return false;
        }
        return true;
    }

    private static boolean isValidTapYield(List<String[]> records) {
        records = records.subList(1, records.size());
        Iterator<String[]> iterator = records.iterator();
        double tapYield;
        String data;
        while (iterator.hasNext()) {
            data = iterator.next()[0];
            if (isValidDouble(data)) {
                tapYield = Double.parseDouble(data);
                if (tapYield <= 0) return false;
            } else return false;
        }
        return true;
    }

    private static List<Double> getTapYield(List<String[]> records) {
        List<Double> tapsYield = new ArrayList<>();
        records = records.subList(1, records.size());
        Iterator<String[]> iterator = records.iterator();
        while (iterator.hasNext()) {
            tapsYield.add(roundData(Double.parseDouble(iterator.next()[0])));
        }
        return tapsYield;
    }

    private static boolean isValidTemperature(List<String[]> records) {
        records = records.subList(1, records.size());
        Iterator<String[]> iterator = records.iterator();
        double temperature;
        String data;
        while (iterator.hasNext()) {
            data = iterator.next()[1];
            if (isValidDouble(data)) {
                temperature = Double.parseDouble(data);
                if (temperature < 1 || temperature > 90) return false;
            } else return false;
        }
        return true;
    }

    private static List<Double> getTemperature(List<String[]> records) {
        List<Double> temperature = new ArrayList<>();
        records = records.subList(1, records.size());
        Iterator<String[]> iterator = records.iterator();
        while (iterator.hasNext()) {
            temperature.add(roundData(Double.parseDouble(iterator.next()[1])));
        }
        return temperature;
    }

    private static boolean isValidSize(List<String[]> records) {
        Iterator<String[]> iterator = records.iterator();
        if (records.size() > 1000001 || records.size() < 2) return false;
        if (iterator.next().length != 1) return false;
        while (iterator.hasNext()) {
            if (iterator.next().length != 2) return false;
        }
        return true;
    }

    private static boolean isValidData(List<String[]> records) {
        if (isValidSize(records) && isValidFuel(records) && isValidTapYield(records) && isValidTemperature(records))
            return true;
        else return false;
    }


    private static double getLoadingTime(double fuel, List<Double> tapYield) {
        double tapYieldSum = 0;
        for (double yield : tapYield) {
            tapYieldSum += yield;
        }
        return roundData(fuel * 60000 / tapYieldSum);
    }

    private static double getAverageTemperature(List<Double> temperature) {
        double sum = 0;
        for (Double temp : temperature) {
            sum += temp;
        }
        return roundData(sum / temperature.size());
    }

    private static void printKlops() {
        System.out.println("klops");
    }
}