import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Paprotka {

    private static final String SPACE_DELIMITER = ",";

    public static void main(String[] args) {
        List<String[]> records = getCSV();
        String row1;
        String[] row2;
        if (isValidData(records)) {
            row1 = records.get(0)[0];
            if (records.size() == 1) {
                System.out.println(row1);
                System.out.println(printMissingFirstFactors(row1));
            } else {
                row2 = records.get(1);
                if (row1.equals("")) {
                    System.out.println(row1);
                    System.out.println(getNumber(row2));
                } else {
                    System.out.println(row1);
                    System.out.println(printMissingFirstFactors(row1, row2));
                }
            }
        } else {
            printKlops();
            return;
        }
    }

    private static List<String[]> getCSV() {
        List<String[]> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(SPACE_DELIMITER);
                records.add(values);
            }
        } catch (IOException e) {
            System.out.println("klops");
        }
        return records;
    }

    private static void printKlops() {
        System.out.println("klops");
    }

    private static boolean isValidSize(List<String[]> records) {
        if (records.size() > 2 || records.size() == 0 || records.get(0).length != 1) return false;
        else return true;
    }

    private static boolean isValidLong(String data) {
        try {
            Long.parseLong(data);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isValidNumber(String data) {
        if (!isValidLong(data) || Long.parseLong(data) <= 1) return false;
        else return true;
    }

    private static boolean isValidFirstFactor(String row1, String[] row2) {
        List<Long> missingFirstFactors = getMissingFirstFactors(row1);
        long number = Long.parseLong(row1);
        if (row2.length > missingFirstFactors.size() || getNumber(row2) > number) return false;
        for (String s : row2) {
            if (!missingFirstFactors.contains(Long.parseLong(s))) return false;
        }
        for (int i = 2; i <= number; i++) {
            if (getOccurrence(row2, i) > getOccurrence(missingFirstFactors, i)) return false;
        }
        if (!isValidFirstFactor(row2)) return false;
        return true;
    }

    private static long getOccurrence(List<Long> list, long number) {
        long occurrence = 0;
        for (Long l : list) {
            if (l == number) occurrence++;
        }
        return occurrence;
    }

    private static long getOccurrence(String[] list, long number) {
        long occurrence = 0;
        for (String s : list) {
            if (Long.parseLong(s) == number) occurrence++;
        }
        return occurrence;
    }

    private static boolean isValidFirstFactor(String[] row2) {
        long number;
        for (String data : row2) {
            if (!isValidNumber(data)) return false;
            number = Long.parseLong(data);
            if (!isValidFirstFactor(number)) return false;
        }
        return true;
    }

    private static boolean isValidFirstFactor(long number) {
        for (long i = 2; i < number; i++) {
            if (number % i == 0) return false;
        }
        return true;
    }

    private static boolean isValidData(List<String[]> records) {
        String row1;
        String[] row2;
        if (!isValidSize(records)) return false;
        row1 = records.get(0)[0];
        if (records.size() == 2) {
            row2 = records.get(1);
            if ((row1.equals("") && isValidFirstFactor(row2)) || (isValidNumber(row1) && isValidFirstFactor(row1, row2)))
                return true;
        } else {
            if (isValidNumber(row1)) return true;
        }

        return false;
    }

    private static long getNumber(String[] row2) {
        long number = 1;
        for (String n : row2) {
            number *= Long.parseLong(n);
        }
        return number;
    }

    private static List<Long> getMissingFirstFactors(String row1) {
        List<Long> missingFirstFactors = new ArrayList<>();
        long number = Long.parseLong(row1);
        for (long i = 2; i <= number; i++) {
            if (number % i == 0 && isValidFirstFactor(i)) {
                missingFirstFactors.add(i);
                number /= i;
                i = 1;
            }
        }
        return missingFirstFactors;
    }

    private static List<Long> getMissingFirstFactors(String row1, String[] row2) {
        List<Long> missingFirstFactors = getMissingFirstFactors(row1);
        long number;
        for (String s : row2) {
            number = Long.parseLong(s);
            if (missingFirstFactors.contains(number)) missingFirstFactors.remove(number);
        }
        return missingFirstFactors;
    }

    private static String printMissingFirstFactors(String row1) {
        List<Long> missingFirstFactors = getMissingFirstFactors(row1);
        return printMissingFirstFactors(missingFirstFactors);
    }

    private static String printMissingFirstFactors(String row1, String[] row2) {
        List<Long> missingFirstFactors = getMissingFirstFactors(row1, row2);
        return printMissingFirstFactors(missingFirstFactors);
    }

    private static String printMissingFirstFactors(List<Long> missingFirstFactors) {
        String factors = Arrays.deepToString(missingFirstFactors.toArray());
        return factors.substring(1, factors.length() - 1).replaceAll(" ", "");
    }
}
