import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Trajektoria {

    public static void main(String[] args) {
        if (isValidData(args)) {
            int amplitude = Integer.parseInt(args[0]);
            int length = Integer.parseInt(args[1]);
            if (amplitude != 1) printTrajectory(amplitude, length);
            else printTrajectory(length);
        } else {
            printKlops();
            return;
        }
    }

    private static void printTrajectory(int length) {
        for (int i = 0; i < length; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    private static void printTrajectory(int amplitude, int length) {
        List<List<String>> trajectory = new ArrayList<>();
        int spaceLengthBefore = amplitude - 1;
        int spaceLengthAfter = 0;
        boolean direction = false;
        for (int row = 1; row <= length; row++) {
            trajectory.add(getRow(spaceLengthBefore, spaceLengthAfter));
            if (direction) {
                spaceLengthBefore++;
                spaceLengthAfter--;
            } else {
                spaceLengthBefore--;
                spaceLengthAfter++;
            }
            if (row % (amplitude - 1) == 0) {
                direction = !direction;
            }
        }
        printTrajectory(transposeTrajectory(trajectory));
    }

    private static void printTrajectory(List<List<String>> transposedTrajectory) {
        for (List<String> r : transposedTrajectory) {
            if (r.contains("*")) {
                String row = Arrays.deepToString(r.toArray());
                int lastIndex = row.lastIndexOf("*");
                System.out.println(row.substring(1, lastIndex + 1).replaceAll(",", ""));
            }
        }
    }

    private static List<List<String>> transposeTrajectory(List<List<String>> trajectory) {
        List<List<String>> transposedTrajectory = new ArrayList<>();
        final int ROWS = trajectory.get(0).size();
        for (int i = ROWS - 1; i >= 0; i--) {
            List<String> row = new ArrayList<>();
            for (List<String> oldRow : trajectory) {
                row.add(oldRow.get(i));
            }
            transposedTrajectory.add(row);
        }
        return transposedTrajectory;
    }

    private static List<String> getRow(int spaceLengthBefore, int spaceLengthAfter) {
        List<String> row = new ArrayList<>();
        for (int i = 0; i < spaceLengthBefore; i++) {
            row.add(" ");
        }
        row.add("*");
        for (int i = 0; i < spaceLengthAfter; i++) {
            row.add(" ");
        }
        return row;
    }

    private static boolean isValidData(String[] args) {
        if (isValidSize(args) && isValidInteger(args) && isValidNumber(args)) return true;
        else return false;
    }

    private static boolean isValidSize(String[] args) {
        if (args.length != 2) return false;
        else return true;
    }

    private static boolean isValidInteger(String[] args) {
        try {
            Integer.parseInt(args[0]);
            Integer.parseInt(args[1]);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isValidNumber(String[] args) {
        for (String s : args) {
            if (Integer.parseInt(s) > 50000 || Integer.parseInt(s) <= 0) return false;
        }
        return true;
    }

    private static void printKlops() {
        System.out.println("klops");
    }
}
